package com.zuitt.example;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class S2A2 {
    public static void main(String[] args) {
        Scanner userInputPrimeNos = new Scanner(System.in);

        int[] intArray = new int[5];

        System.out.println("Input 1st of 5 prime numbers:");
        int primeOne = userInputPrimeNos.nextInt();
        if(primeOne > 1 && primeOne % primeOne == 0  && primeOne % 1 == 0){
//        if(isPrim){
            System.out.println(primeOne + " is a prime number");
        }
        else{
            System.out.println(primeOne + " is NOT a prime number");
        }

        String[] names = {"John", "Jane", "Chloe", "Zoey"};
        //System.out.println("My friends are: " + Arrays.toString(names));

        ArrayList<String> friends = new ArrayList<String>(Arrays.asList(names));
        System.out.println("My friends are: " + friends);
    }
}
