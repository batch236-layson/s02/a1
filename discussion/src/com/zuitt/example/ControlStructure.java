package com.zuitt.example;

import java.util.Scanner;

public class ControlStructure {
    public static void main(String[] args){
        //[SECTION] Java Operators
            //Arithmetic -> +, -, *, /, %
            //Comparison -> >, <, >=, <=, ==, !=, ===, !==
            //Assignment -> =

        //[SECTION] Selection Control Structure in Java
            //if else
        int num = 36;
        if(num % 5 == 0){
            System.out.println(num + " is divisible by 5");
        }
        else{
            System.out.println(num + " is not divisible by 5");
        }

        //[SECTION] Short Circuiting
            //This is helpful to prevent run time errors
        int x = 15;
        int y = 0;

        if(y != 0 && x/y == 0){
            System.out.println("Result is: " + x/y);
        }

        //[SECTION] Ternary Ooperator
        int num1 = 24;
        Boolean result = (num1 > 0) ? true: false;
        System.out.println(result);

        //[SECTION] Switch Cases
        Scanner numberScanner = new Scanner(System.in);
        System.out.println("Enter a number: ");
        int directionValue = numberScanner.nextInt();

        switch (directionValue){
            case 1:
                System.out.println("North");
                break;
            case 2:
                System.out.println("South");
            case 3:
                System.out.println("West");
            case 4:
                System.out.println("East");
            default:
                System.out.println("Invalid");
        }
    }
}