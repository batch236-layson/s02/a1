package com.zuitt.example;
import java.util.Scanner;
public class S2A1 {
    public static void main(String[] args){
        Scanner leapYearScanner = new Scanner(System.in);
        System.out.println("Input year to be checked if it is a leap year:");
        int providedYear = leapYearScanner.nextInt();

        if((providedYear % 4 == 0) && (providedYear % 100 != 0) || (providedYear % 400 == 0)){
            System.out.println(providedYear + " is a leap year");
//            if(providedYear % 100 != 0){
//                if(providedYear % 400 == 0){
//                    System.out.println(providedYear + " is a leap year");
//                }
//                else{
//                    System.out.println(providedYear + " is NOT a leap year");
//                }
//            }else{
//                System.out.println(providedYear + " is NOT a leap year");
//            }
        }else{
            System.out.println(providedYear + " is NOT a leap year");
        }

//        int num = 36;
//        if(num % 5 == 0){
//            System.out.println(num + " is divisible by 5");
//        }
//        else{
//            System.out.println(num + " is not divisible by 5");
//        }
//
//        int num1 = 24;
//        Boolean result = (num1 > 0) ? true: false;
//        System.out.println(result);
    }
}
